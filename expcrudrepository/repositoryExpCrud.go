package expcrudrepository

import (
	"database/sql"
	"expcrud/expcrudconfig"
	"expcrud/expcrudmodel"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Create with last insert id. use db.QueryRow()
func CreateExpCrudWithId(req expcrudmodel.RequestExpCrud) (int, string) {
	var id int
	db := expcrudconfig.DbConn()
	defer db.Close()
	// Get curr date time
	t := time.Now()
	dbTime := t.Format("2006-01-02 15:04:05")
	err := db.QueryRow(`INSERT INTO produk(namaproduk, created_at, harga)
		VALUES($1, $2, $3) RETURNING id`, req.NamaProduk, dbTime, req.Harga).Scan(&id)
	if err != nil {
		// Error handler
		return id, err.Error()
	}
	return id, ""
}

// Create without last insert id. use db.Exec()
func CreateExpCrudWithoutId(req expcrudmodel.RequestExpCrud) (bool, string) {
	db := expcrudconfig.DbConn()
	fmt.Println("req di repo: ", req)
	defer db.Close()
	// Get curr date time
	t := time.Now()
	dbTime := t.Format("2006-01-02 15:04:05")
	_, err := db.Exec(`INSERT INTO produk(namaproduk, created_at, harga) values ($1, $2, $3)`, req.NamaProduk, dbTime, req.Harga)
	if err != nil {
		// Error handler
		return false, err.Error()
	}
	return true, ""
}

// Select, single data. use db.QueryRow()
func SelectExpCrudSingle(id int) (expcrudmodel.IsiResponseExpCrud, string) {
	var result expcrudmodel.IsiResponseExpCrud
	db := expcrudconfig.DbConn()
	defer db.Close()
	err := db.QueryRow(`SELECT * FROM produk WHERE deleted_at IS NULL and id = $1`, id).
		Scan(&result.ID, &result.NamaProduk, &result.CreatedAt, &result.UpdatedAt, &result.DeletedAt, &result.Harga)
	if err == sql.ErrNoRows {
		//log.Fatal("No Results Found")
		return result, "No Results Found"
	}
	if err != nil {
		return result, err.Error()
	}
	return result, ""
}

// Select, Many data. use db.Query()
func SelectExpCrudMany(offset int, limit int) ([]expcrudmodel.IsiResponseExpCrud, string) {
	var result []expcrudmodel.IsiResponseExpCrud
	db := expcrudconfig.DbConn()
	defer db.Close()
	rows, err := db.Query(`SELECT * FROM produk WHERE deleted_at IS NULL ORDER BY id DESC LIMIT $1 OFFSET $2`, limit, offset)
	if err != nil {
		return result, err.Error()
	}
	defer rows.Close()
	for rows.Next() {
		var prd expcrudmodel.IsiResponseExpCrud
		rows.Scan(&prd.ID, &prd.NamaProduk, &prd.CreatedAt, &prd.UpdatedAt, &prd.DeletedAt, &prd.Harga)
		result = append(result, prd)
	}
	// Get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return result, err.Error()
	}
	return result, ""
}

// Update data. use db.Exec()
func UpdateExpCrud(req expcrudmodel.RequestExpCrudUpdate) (bool, string) {
	db := expcrudconfig.DbConn()
	defer db.Close()
	// Get curr date time
	t := time.Now()
	dbTime := t.Format("2006-01-02 15:04:05")
	_, err := db.Exec(`UPDATE produk set namaproduk = $1, updated_at = $2, harga = $3 WHERE id=$4`, req.NamaProduk, dbTime, req.Harga, req.ID)
	if err != nil {
		// Error handler
		return false, err.Error()
	}
	return true, ""
}

// Soft Delete. use db.Exec()
func SoftDeleteExpCrud(req expcrudmodel.RequestExpCrudDelete) (bool, string) {
	db := expcrudconfig.DbConn()
	defer db.Close()
	// Get curr date time
	t := time.Now()
	dbTime := t.Format("2006-01-02 15:04:05")
	_, err := db.Exec(`UPDATE produk set deleted_at = $1 WHERE id=$2`, dbTime, req.ID)
	if err != nil {
		// Error handler
		return false, err.Error()
	}
	return true, ""
}

// Delete data. use db.Exec()
func DeleteExpCrud(req expcrudmodel.RequestExpCrudDelete) (bool, string) {
	db := expcrudconfig.DbConn()
	defer db.Close()
	_, err := db.Exec(`DELETE FROM produk WHERE id=$1`, req.ID)
	if err != nil {
		// Error handler
		return false, err.Error()
	}
	return true, ""
}

// Delete data. use db.Exec()
func CreateExpCrudBulk(req []*expcrudmodel.RequestExpCrudJson) (bool, string) {
	vals := []interface{}{}
	for _, row := range req {
		vals = append(vals, row.NamaProduk, row.Harga)
	}

	sqlStr := `INSERT INTO produk(namaproduk, harga) VALUES %s`
	sqlStr = ReplaceSQL(sqlStr, "(?, ?)", len(req))

	db := expcrudconfig.DbConn()
	defer db.Close()
	_, err := db.Exec(sqlStr, vals...)
	if err != nil {
		// Error handler
		return false, err.Error()
	}
	return true, ""
}

func ReplaceSQL(stmt, pattern string, len int) string {
	pattern += ","
	stmt = fmt.Sprintf(stmt, strings.Repeat(pattern, len))
	n := 0
	for strings.IndexByte(stmt, '?') != -1 {
		n++
		param := "$" + strconv.Itoa(n)
		stmt = strings.Replace(stmt, "?", param, 1)
	}
	return strings.TrimSuffix(stmt, ",")
}
