package expcrudrepository

import (
	"expcrud/expcrudconfig"
	"expcrud/expcrudmodel"
)

func TestConn() expcrudmodel.ResultTestConnection {
	var result expcrudmodel.ResultTestConnection
	var id int
	db := expcrudconfig.DbConn()
	defer db.Close()
	err := db.QueryRow("SELECT id FROM produk").Scan(&id)
	if err != nil {
		result.Status = 201
		result.Result = id
		result.Error.Msg = err.Error()
		return result
	}
	result.Status = 200
	result.Result = id
	return result
}
