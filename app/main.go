package main

import (
	"expcrud/expcrudmodel"
	"expcrud/expcrudrepository"
	"expcrud/expcrudusecase"
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())

	e.HTTPErrorHandler = func(err error, c echo.Context) {
		report, ok := err.(*echo.HTTPError)
		if !ok {
			report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		errhandler := new(expcrudmodel.ErrorHttpHandler)
		errhandler.Status = report.Code
		errhandler.Result = false
		errhandler.Error.Msg = fmt.Sprintf("%v", report.Message)
		errhandler.Error.Status = "10035x" + fmt.Sprintf("%v", report.Code)
		c.Logger().Error(report)
		c.JSON(report.Code, errhandler)
	}

	e.GET("/heroku/api/v1/tc", func(c echo.Context) error {
		res := expcrudrepository.TestConn()
		return c.JSON(http.StatusOK, res)
	})

	e.POST("/heroku/api/v1/expcrud/createbulk", func(c echo.Context) error {
		u := new([]*expcrudmodel.RequestExpCrudJson)
		if err := c.Bind(u); err != nil {
			return c.JSON(http.StatusOK, err.Error())
		}

		result := expcrudusecase.CreateExpCrudBulk(*u)
		return c.JSON(http.StatusOK, result)
	})

	e.POST("/heroku/api/v1/expcrud/create", func(c echo.Context) error {
		u := new(expcrudmodel.RequestExpCrud)
		if err := c.Bind(u); err != nil {
			return c.JSON(http.StatusOK, err.Error())
		}
		fmt.Println("hasilnya u:", u)
		if err := c.Validate(u); err != nil {
			res := new(expcrudmodel.ResponseValidate)
			res.Status = 204
			res.Result = false
			res.Error.Msg = err.Error()
			res.Error.Status = "10030x204"
			return c.JSON(http.StatusOK, res)
		} else {
			result := expcrudusecase.CreateExpCrud(*u)
			return c.JSON(http.StatusOK, result)
		}
	})

	e.POST("/heroku/api/v1/expcrud/createedan", func(c echo.Context) error {
		u := new(expcrudmodel.RequestExpCrud)
		if err := c.Bind(u); err != nil {
			return c.JSON(http.StatusOK, err.Error())
		}
		if err := c.Validate(u); err != nil {
			res := new(expcrudmodel.ResponseValidate)
			res.Status = 204
			res.Result = false
			res.Error.Msg = err.Error()
			res.Error.Status = "10030x204"
			return c.JSON(http.StatusOK, res)
		} else {
			result := expcrudusecase.CreateExpCrudEdan(*u)
			return c.JSON(http.StatusOK, result)
		}
	})

	e.GET("/heroku/api/v1/expcrud/select/:id", func(c echo.Context) error {
		var id, _ = strconv.Atoi(c.Param("id"))
		result := expcrudusecase.SelectExpCrudSingle(id)
		return c.JSON(http.StatusOK, result)
	})

	e.GET("/heroku/api/v1/expcrud/list/:offset/:limit", func(c echo.Context) error {
		var offset, _ = strconv.Atoi(c.Param("offset"))
		var limit, _ = strconv.Atoi(c.Param("limit"))
		result := expcrudusecase.SelectExpCrudMany(offset, limit)
		return c.JSON(http.StatusOK, result)
	})

	e.POST("/heroku/api/v1/expcrud/update", func(c echo.Context) error {
		u := new(expcrudmodel.RequestExpCrudUpdate)
		if err := c.Bind(u); err != nil {
			return c.JSON(http.StatusOK, err.Error())
		}
		if err := c.Validate(u); err != nil {
			res := new(expcrudmodel.ResponseValidate)
			res.Status = 204
			res.Result = false
			res.Error.Msg = err.Error()
			res.Error.Status = "10030x204"
			return c.JSON(http.StatusOK, res)
		} else {
			result := expcrudusecase.UpdateExpCrud(*u)
			return c.JSON(http.StatusOK, result)
		}
	})

	e.POST("/heroku/api/v1/expcrud/softdelete", func(c echo.Context) error {
		u := new(expcrudmodel.RequestExpCrudDelete)
		if err := c.Bind(u); err != nil {
			return c.JSON(http.StatusOK, err.Error())
		}
		if err := c.Validate(u); err != nil {
			res := new(expcrudmodel.ResponseValidate)
			res.Status = 204
			res.Result = false
			res.Error.Msg = err.Error()
			res.Error.Status = "10030x204"
			return c.JSON(http.StatusOK, res)
		} else {
			result := expcrudusecase.SoftDeleteExpCrud(*u)
			return c.JSON(http.StatusOK, result)
		}
	})
	//port := os.Getenv("PORT")
	// Running in port : 10035
	e.Logger.Fatal(e.Start(":1000"))

}
