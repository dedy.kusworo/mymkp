package expcrudusecase

import (
	"expcrud/expcrudmodel"
	"expcrud/expcrudrepository"
	"fmt"
)

// Create
func CreateExpCrudBulk(req []*expcrudmodel.RequestExpCrudJson) expcrudmodel.ResponseExpCrudStatus {
	var result expcrudmodel.ResponseExpCrudStatus
	res, errrepo := expcrudrepository.CreateExpCrudBulk(req)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
	} else {
		result.Status = 200
		result.Result = res
	}
	return result
}

// Create
func CreateExpCrud(req expcrudmodel.RequestExpCrud) expcrudmodel.ResponseExpCrudStatus {
	var result expcrudmodel.ResponseExpCrudStatus
	res, errrepo := expcrudrepository.CreateExpCrudWithoutId(req)
	fmt.Println("hasil dari req: ", req)
	fmt.Println("hasil dari res repository: ", res)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
	} else {
		result.Status = 200
		result.Result = res
	}
	return result
}

// Create
func CreateExpCrudEdan(req expcrudmodel.RequestExpCrud) expcrudmodel.ResponseExpCrudStatus {
	var result expcrudmodel.ResponseExpCrudStatus
	sum := 0
	for i := 0; i < 1000000; i++ {
		sum += i
		expcrudrepository.CreateExpCrudWithoutId(req)
	}
	result.Status = 200
	return result
}

// Select single data
func SelectExpCrudSingle(id int) expcrudmodel.ResponseExpCrud {
	var result expcrudmodel.ResponseExpCrud
	res, errrepo := expcrudrepository.SelectExpCrudSingle(id)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
	} else {
		result.Status = 200
		result.Result = res
	}
	return result
}

// Select many data
func SelectExpCrudMany(offset int, limit int) expcrudmodel.ResponseExpCrudMany {
	var result expcrudmodel.ResponseExpCrudMany
	listdata, errrepo := expcrudrepository.SelectExpCrudMany(offset, limit)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
		return result
	}
	result.Status = 200
	result.Result = listdata
	return result
}

// Update data
func UpdateExpCrud(req expcrudmodel.RequestExpCrudUpdate) expcrudmodel.ResponseExpCrudStatus {
	var result expcrudmodel.ResponseExpCrudStatus
	res, errrepo := expcrudrepository.UpdateExpCrud(req)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
	} else {
		result.Status = 200
		result.Result = res
	}
	return result
}

// Soft delete data
func SoftDeleteExpCrud(req expcrudmodel.RequestExpCrudDelete) expcrudmodel.ResponseExpCrudStatus {
	var result expcrudmodel.ResponseExpCrudStatus
	res, errrepo := expcrudrepository.SoftDeleteExpCrud(req)
	if errrepo != "" {
		result.Status = 204
		result.Error.Status = "10035x204"
		result.Error.Msg = errrepo
	} else {
		result.Status = 200
		result.Result = res
	}
	return result
}
