package expcrudmodel

//package expcrudmodel

import "time"

// Req expcrud create
type RequestExpCrud struct {
	NamaProduk string `validate:"required,max=127" json:"namaproduk"`
	Harga      int    `validate:"required,min=1000" json:"harga"`
}

// Req expcrud create
type RequestExpCrudJson struct {
	NamaProduk string `json:"namaproduk"`
	Harga      int    `json:"harga"`
}

// Req expcrud update
type RequestExpCrudUpdate struct {
	ID         int    `validate:"required"`
	NamaProduk string `validate:"required,max=127"`
	Harga      int    `validate:"required,min=1000"`
}

// Req expcrud update
type RequestExpCrudDelete struct {
	ID int `validate:"required"`
}

// Response expcrud true or false
type ResponseExpCrudStatus struct {
	Status int      `json:"status"`
	Result bool     `json:"result"`
	Error  ErrorMsg `json:"error"`
}

// Response expcrud single
type ResponseExpCrud struct {
	Status int                `json:"status"`
	Result IsiResponseExpCrud `json:"result"`
	Error  ErrorMsg           `json:"error"`
}

// Response expcrud many
type ResponseExpCrudMany struct {
	Status int                  `json:"status"`
	Result []IsiResponseExpCrud `json:"result"`
	Error  ErrorMsg             `json:"error"`
}

// Result expcrud
type IsiResponseExpCrud struct {
	ID         int        `json:"id"`
	NamaProduk string     `json:"namaproduk"`
	Harga      int        `json:"harga"`
	CreatedAt  time.Time  `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
	DeletedAt  *time.Time `json:"deleted_at"`
}
