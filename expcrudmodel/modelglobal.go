package expcrudmodel

// VALIDATED
type ResponseValidate struct {
	Status int      `json:"status"`
	Result bool     `json:"result"`
	Error  ErrorMsg `json:"error"`
}

// ERROR HANDLING
type ErrorHttpHandler struct {
	Status int      `json:"status"`
	Result bool     `json:"result"`
	Error  ErrorMsg `json:"error"`
}

// GLOBAL ERROR MSG
type ErrorMsg struct {
	Status string `json:"status"`
	Msg    string `json:"msg"`
}

// TEST CONN
type ResultTestConnection struct {
	Status int      `json:"status"`
	Result int      `json:"result"`
	Error  ErrorMsg `json:"error"`
}
