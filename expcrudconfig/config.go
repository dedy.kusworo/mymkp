package expcrudconfig

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

//=== ENVIRONMENT
func DbConn() *sql.DB {
	//url := "postgres://dklsusakdmatdz:43488298ee57002051e4a36e676b3d290f2ba22d9b1d3a4744231e8808790426@ec2-3-215-83-17.compute-1.amazonaws.com:5432/d6onf5tgis120c"
	// url := os.Getenv("DATABASE_URL")
	// connection, _ := pq.ParseURL(url)
	// connection += " sslmode=require"
	// db, err := sql.Open("postgres", connection)

	db, err := sql.Open("postgres", "user=postgres password=1234 dbname=expcrud port=5432 sslmode=disable")
	if err != nil {
		fmt.Println(err.Error())
		//log.Fatal(err)
	}
	return db
}
